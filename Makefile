ALL : code

code : code.o

% : %.o
	$(CXX) $< -o $@

%.o : %.cpp
	$(CXX) -c $< -o $@

clean:
	$(RM) code *.o
